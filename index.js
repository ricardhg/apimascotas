

const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors());

app.get('/', function (req, res) {
    res.send('Api mascotas')
})

const modelos = require('./models/index');

app.get('/gatos', function (req, res) {
    modelos.Gato.findAll()
        .then(gatos => res.json(gatos))
        .catch(error => res.json(error))
});

app.listen(3005, function () {
    console.log('Escuchando en puerto 3005!')
})

