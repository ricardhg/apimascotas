
module.exports = (sequelize, DataTypes) => {
  const Gato = sequelize.define('Gato', {

    nombre: DataTypes.STRING,
    raza: DataTypes.STRING,
    anyo: DataTypes.INTEGER,
    foto: DataTypes.STRING,
    
  }, { tableName: 'gatos', timestamps: false});
  
  return Gato;
};
