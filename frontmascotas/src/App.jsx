import { useState, useEffect } from 'react'
import './App.css'

function App() {

  const [gatos, setGatos] = useState([]);

  useEffect( ()=>{

    fetch("http://localhost:3005/gatos")
    .then(datos => datos.json())
    .then(datosgatos => setGatos(datosgatos))
    .catch(error => console.log(error))

  }, [])
  
  return (
    <div>
      <h1>Gatos de la API</h1>
      <ul>
        {gatos.map((g, i) => <li key={i}>{g.nombre}</li>)}
      </ul>

    </div>
  )
}

export default App
